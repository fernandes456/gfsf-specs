Pod::Spec.new do |s|
	s.name = "app-message-ios"
	s.version = "3.6.1"
	s.summary = "An elegant messages UI library for iOS."
	s.homepage = "https://github.com/elo7/app-message-ios"
	s.authors = "Elo7"
	s.license = "MIT"
	s.platform = :ios, "9.0"
    s.xcconfig = { 'SWIFT_VERSION' => '3.0' }

	s.source = { :git => "git@github.com:elo7/app-message-ios.git", :tag => s.version }
	s.source_files = "Elo7MessagesViewController/**/*.{h,m}"

s.resources = ["Elo7MessagesViewController/Assets/Elo7MessagesAssets.bundle", "Elo7MessagesViewController/**/*.{xib}", "Elo7MessagesViewController/**/*.{storyboard}", "Elo7MessagesViewController/**/*.{xcdatamodeld}", "Elo7MessagesViewController/Elo7.xcassets", "Elo7MessagesViewController/Supporting Files/Localizable.stringsdict", "Elo7MessagesViewController/Supporting Files/Elo7Messages-Prefix.pch"]
	
	s.frameworks = "QuartzCore", "CoreGraphics", "CoreLocation", "MapKit", "UIKit", "Foundation"
	s.requires_arc = true
s.prefix_header_contents = '#ifdef DEBUG
#define DLog(...) NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])
#define ALog(...) [[NSAssertionHandler currentHandler] handleFailureInFunction:[NSString stringWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] file:[NSString stringWithCString:__FILE__ encoding:NSUTF8StringEncoding] lineNumber:__LINE__ description:__VA_ARGS__]
#else
#define DLog(...) do { } while (0)
#ifndef NS_BLOCK_ASSERTIONS
#define NS_BLOCK_ASSERTIONS
#endif
#define ALog(...) NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])
#endif
#define ZAssert(condition, ...) do { if (!(condition)) { ALog(__VA_ARGS__); }} while(0)'
	s.dependency "JSQSystemSoundPlayer", "~> 2.0.1"
	s.dependency "QBImagePickerController", "~> 3.0.0"
    s.dependency 'SDWebImage', '~> 3.7'
    s.dependency 'QBImagePickerController', '~> 3.0'
    #s.dependency 'Artsy+OSSUIFonts', '1.1.0'
    s.dependency 'app-networking-ios', '2.7.0'

end
