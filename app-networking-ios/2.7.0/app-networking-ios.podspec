Pod::Spec.new do |s|
s.name = "app-networking-ios"
s.version = "2.7.0"
s.summary = "Elo7 network library"
s.homepage = "https://github.com/elo7/app-networking-ios/"
s.authors = "Elo7"
s.license = "MIT"
s.platform = :ios, "9.0"
s.xcconfig = { 'SWIFT_VERSION' => '3.0' }

s.source = { :git => "git@github.com:elo7/app-networking-ios.git", :tag => s.version }
s.source_files = "app-networking-ios/*/*.{h,m}"

s.resources = ["app-networking-ios/Images.xcassets"]

s.frameworks = "Foundation"
s.requires_arc = true
s.prefix_header_contents = '#ifdef DEBUG
#define DLog(...) NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])
#define ALog(...) [[NSAssertionHandler currentHandler] handleFailureInFunction:[NSString stringWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] file:[NSString stringWithCString:__FILE__ encoding:NSUTF8StringEncoding] lineNumber:__LINE__ description:__VA_ARGS__]
#else
#define DLog(...) do { } while (0)
#ifndef NS_BLOCK_ASSERTIONS
#define NS_BLOCK_ASSERTIONS
#endif
#define ALog(...) NSLog(@"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])
#endif
#define ZAssert(condition, ...) do { if (!(condition)) { ALog(__VA_ARGS__); }} while(0)'
s.dependency "AFNetworking", "~> 2.5"
s.dependency "SVProgressHUD", "~> 1.1.2"
s.dependency "MQTTClient", '0.8.5'
end
